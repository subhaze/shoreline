package docker

import (
	"fmt"
	"os"
	"path"
	"regexp"
	"strconv"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/events"
	"github.com/docker/docker/client"
	"golang.org/x/net/context"
)

// var client *docker.Client

func getClient() *client.Client {
	// ctx := context.Background()
	cli, err := client.NewEnvClient()
	if err != nil {
		panic(err)
	}
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return cli
}

// GetContainerWebSocket : return a socket connection to container
func GetContainerWebSocket(id string) (types.HijackedResponse, error) {
	return getClient().ContainerAttach(context.Background(), id, types.ContainerAttachOptions{
		Stream: true,
	})
}

// GetEventStream : return streams for API to emit to frontend
func GetEventStream() (<-chan events.Message, <-chan error) {
	return getClient().Events(context.Background(), types.EventsOptions{})
}

func localPortToString(ports []types.Port) string {
	for _, port := range ports {
		if port.PublicPort != 0 {
			return strconv.Itoa(int(port.PublicPort))
		}
	}
	return ""
}

// ContainerToString : Allows printing of useful info about container
func ContainerToString(container types.Container) string {
	fmt.Println("PORTS: ", container.Ports)
	port := localPortToString(container.Ports)
	containerStr := "URL:            " + "http://localhost:" + port
	containerStr += "\nID:             " + container.ID[0:12]
	containerStr += "\nName:           " + strings.TrimPrefix(container.Names[0], "/")
	// containerStr += "\nLabels:"
	// for _, label := range container.Labels {
	// 	containerStr += "\n  " + label
	// }
	containerStr += "\nDocker Networks:"
	fmt.Println(container.NetworkSettings)
	for networkLabel, network := range container.NetworkSettings.Networks {
		containerStr += "\n Localhost: -- " + networkLabel
		containerStr += "\n Gateway: ---- " + network.Gateway
		containerStr += "\n Docker IP: -- " + network.IPAddress
		containerStr += "\n ------------- "
	}
	containerStr += "\nMounts:"
	for _, mount := range container.Mounts {
		containerStr += "\n  Source: ------- " + mount.Source
		containerStr += "\n  Destination: -- " + mount.Destination
		containerStr += "\n  Mode: -- " + mount.Mode
		containerStr += "\n ---------------- "
	}
	// containerStr += "Created: " + container.Created
	// 	fmt.Println("Command: ", container.Command)
	containerStr += "\nImage:          " + container.Image
	containerStr += "\nState:          " + container.State
	containerStr += "\nStatus:         " + container.Status
	containerStr += "\nLocal Port:     " + port
	return containerStr
}

// GetRunningContainers : Returns list of all running containers
func GetRunningContainers() []types.Container {
	containers, err := getClient().ContainerList(context.Background(), types.ContainerListOptions{})
	if err != nil {
		panic(err)
	}
	return containers
}

// IsNetworkRunning : Given a network name checks whether it's running or not
func IsNetworkRunning(name string) bool {
	for _, container := range GetRunningContainers() {
		for networkLabel := range container.NetworkSettings.Networks {
			if name == networkLabel {
				return true
			}
		}
	}
	return false
}

// GetContainerName : Given a container name returns that container object
func GetContainerName(container types.Container) string {
	if len(container.Names) > 0 {
		return strings.TrimPrefix(container.Names[0], "/")
	}
	return ""
}

var containerStats map[string]interface{}

// GetContainerStats : Given a container id returns stats for it
func GetContainerStats(containerID string) types.ContainerStats {
	stats, _ := getClient().ContainerStats(context.Background(), containerID, true)
	return stats
}

// GetContainerMountPoint : Given a container attempts to determine it's mount point.
// If there are multiple mount points it will only return the first
func GetContainerMountPoint(container types.Container) string {
	if len(container.Mounts) > 0 {
		return container.Mounts[0].Source
	}
	return ""
}

func dockerIsWatchPointContainer(container types.Container) bool {
	return strings.Contains(container.Image, "watchpoint")
}

// FindContainerFromMountPoint : Attempts to find a running container based on a folder path
func FindContainerFromMountPoint(path string) (types.Container, bool) {
	for _, container := range GetRunningContainers() {
		for _, mount := range container.Mounts {
			if mount.Source == path && !dockerIsWatchPointContainer(container) {
				return container, true
			}
		}
	}
	return types.Container{}, false
}

// FindContainersFromMountPoint : Attempts to find all running containers related to a folder path
// The method is assuming the container names will have the same prefix as the container associated
// to the mount path.
func FindContainersFromMountPoint(path string) (types.Container, bool) {
	for _, container := range GetRunningContainers() {
		for _, mount := range container.Mounts {
			if mount.Source == path && !dockerIsWatchPointContainer(container) {
				return container, true
			}
		}
	}
	return types.Container{}, false
}

// GetContainerDetails : Returns details about the running container or an error
func GetContainerDetails(name string) (types.ContainerJSON, error) {
	return getClient().ContainerInspect(context.Background(), name)
}

// GetContainerEnvString : Return a containers env variables in the format:
// -e key=value -e key2=value2 ...etc
func GetContainerEnvString(containerName string) string {
	containerEnvs := ""
	c, _ := GetContainerDetails(containerName)

	for _, envVar := range c.Config.Env {
		if strings.HasPrefix(envVar, "PATH") {
			break
		}
		containerEnvs = containerEnvs + fmt.Sprintf("-e '%s' ", envVar)
	}
	return containerEnvs
}

// GetBaseContainerNamesFromPath : Normalizes the base folder of provided path to
// same format as docker-compose usage
// Source: https://github.com/docker/compose/blob/7ae632a9ee7530fcf81e212baa3e588f477ea862/compose/cli/command.py#L131
func GetBaseContainerNamesFromPath(p string) string {
	base := path.Base(p)
	re, err := regexp.Compile("[^-_a-z0-9]+")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return re.ReplaceAllString(strings.ToLower(base), "")
}
