package fs

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"path/filepath"
	"time"

	bolt "go.etcd.io/bbolt"
)

func bucketShorelineProject() []byte {
	return []byte("shoreline.projects")
}

func getDB() (*bolt.DB, error) {
	db, err := bolt.Open(filepath.Join(UserHomeDir(), ".shoreline.db"), 0600, &bolt.Options{Timeout: 1 * time.Second})
	bucketErr := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(bucketShorelineProject())
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	})
	if bucketErr != nil {
		err = bucketErr
	}
	return db, err
}

// CreateProject : Save a project to file
func CreateProject(projectPath string) error {
	projects := GetProjects()
	for _, project := range projects {
		if project.Path == projectPath {
			return errors.New("Project already exists at: " + projectPath)
		}
	}
	db, err := getDB()
	defer db.Close()
	if err != nil {
		log.Fatal(err)
		return err
	}
	db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketShorelineProject())
		id, _ := b.NextSequence()
		project := savedProject{
			ID:   int(id),
			path: projectPath,
		}
		buf, err := json.Marshal(project)
		if err != nil {
			return err
		}
		return b.Put(itob(project.ID), buf)
	})
	return nil
}

// itob returns an 8-byte big endian representation of v.
func itob(v int) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(v))
	return b
}

// GetProjects : Get slice of projects saved
func GetProjects() []SitesFoldersWithComposeStruct {
	projects := []SitesFoldersWithComposeStruct{}
	db, err := getDB()
	defer db.Close()
	if err != nil {
		log.Fatal(err)
		return projects
	}
	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketShorelineProject())
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			sp := savedProject{}
			err := json.Unmarshal(v, &sp)
			if err != nil {
				return err
			}
			project, err := FolderParseComposeFile(sp.path)
			if err != nil {
				return err
			}
			projects = append(projects, project)
		}
		return nil
	})
	return projects
}
