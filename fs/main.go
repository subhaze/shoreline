package fs

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"shoreline/docker"
	"strings"

	"github.com/docker/docker/api/types"
	homedir "github.com/mitchellh/go-homedir"
	yaml "gopkg.in/yaml.v2"
)

// BaseFolderStruct : Base folder
type BaseFolderStruct struct {
	Name       string `json:"name"`
	Path       string `json:"path"`
	HasCompose bool   `json:"hasCompose"`
	Hidden     bool   `json:"hidden"`
}

// SitesFoldersWithComposeStruct : Simple struct for attaching compose data to folder
type SitesFoldersWithComposeStruct struct {
	Name        string `json:"name"`
	Path        string `json:"path"`
	ComposeFile string `json:"composeFile"`
	Compose     YamlDockerComposeStruct
	Containers  []types.ContainerJSON `json:"containers"`
	// ComposeNestedMap YamlDockerComposeMappedStruct
}

// YamlDockerComposeStruct : test
type YamlDockerComposeStruct struct {
	Services map[string]service
	Networks struct {
		Default struct {
			External struct {
				Name string
			}
		}
	}
}

type service struct {
	Image       string
	DependsOn   []string          `yaml:"depends_on" default:"[]"`
	Ports       []string          `default:"[]"`
	Environment map[string]string `default:"[]"`
	Volumes     []string          `default:"[]"`
}

type savedProject struct {
	ID   int
	path string
}

// CurrentWorkingDir : Returns the current users home directory or exits with error if not found
func CurrentWorkingDir() string {
	cwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return cwd
}

// PathExists : Simple check to see if a folder or file exists
func PathExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return true
}

// FoldersFromPath : Get a list of folders from path
func FoldersFromPath(path string) []BaseFolderStruct {
	folders := []BaseFolderStruct{}
	pathExists := PathExists(path)
	if !pathExists {
		return folders
	}
	paths, err := ioutil.ReadDir(path)
	if err != nil {
		return folders
	}
	for _, folder := range paths {
		if folder.IsDir() {
			folders = append(folders, BaseFolderStruct{
				Name:       folder.Name(),
				Path:       path,
				Hidden:     strings.HasPrefix(folder.Name(), "."),
				HasCompose: PathExists(filepath.Join(path, folder.Name(), "docker-compose.yml")),
			})
		}
	}
	return folders
}

// SitesFolders : Returns list of folders found within '~/Sites'
func SitesFolders() []string {
	sitesFolderPath := filepath.Join(UserHomeDir(), "Sites")
	hasSitesFolder := PathExists(sitesFolderPath)
	folderNames := []string{}
	if !hasSitesFolder {
		fmt.Println("no folder: " + sitesFolderPath)
		return folderNames
	}
	paths, err := ioutil.ReadDir(sitesFolderPath)
	if err != nil {
		fmt.Println("err")
		return folderNames
	}
	for _, path := range paths {
		if path.IsDir() && !strings.HasPrefix(path.Name(), ".") {
			folderNames = append(folderNames, path.Name())
		}
	}
	return folderNames
}

// SitesFoldersWithCompose : Returns folders found within '~/Sites' with their compose data attached
func SitesFoldersWithCompose() []SitesFoldersWithComposeStruct {
	sitesFolderPath := filepath.Join(UserHomeDir(), "Sites")
	folders := []SitesFoldersWithComposeStruct{}

	for _, folderName := range SitesFolders() {
		folderPath := filepath.Join(sitesFolderPath, folderName)
		composeFilePath := filepath.Join(folderPath, "docker-compose.yml")
		if PathExists(composeFilePath) {
			project := SitesFoldersWithComposeStruct{
				Name:        folderName,
				Path:        folderPath,
				ComposeFile: composeFilePath,
				Compose:     YamlParseDockerCompose(composeFilePath),
				Containers:  []types.ContainerJSON{},
			}
			for _, container := range docker.GetRunningContainers() {
				if container.Labels["com.docker.compose.project"] == project.Name {
					c, err := docker.GetContainerDetails(container.ID)
					if err == nil {
						project.Containers = append(project.Containers, c)
					}
				}
			}
			folders = append(folders, project)
		}
	}
	return folders
}

// FolderParseComposeFile : Look for and parse docker compose file into struct
func FolderParseComposeFile(path string) (SitesFoldersWithComposeStruct, error) {
	composeFilePath := filepath.Join(path, "docker-compose.yml")
	if PathExists(composeFilePath) {
		return SitesFoldersWithComposeStruct{
			Name:        filepath.Base(path),
			Path:        path,
			ComposeFile: composeFilePath,
			Compose:     YamlParseDockerCompose(composeFilePath),
		}, nil
	}
	return SitesFoldersWithComposeStruct{}, errors.New("Could not find compose file at: " + path)
}

// SitesFoldersWithComposeRunning : Returns list of folders that have a running container along with compose struct
func SitesFoldersWithComposeRunning() []SitesFoldersWithComposeStruct {
	sites := SitesFoldersWithCompose()
	folders := []SitesFoldersWithComposeStruct{}
	for _, site := range sites {
		_, running := docker.FindContainerFromMountPoint(site.Path)
		if running {
			folders = append(folders, site)
		}
	}
	return folders
}

// SitesFoldersWithComposeStopped : Returns list of folders that have a stopped container along with compose struct
func SitesFoldersWithComposeStopped() []SitesFoldersWithComposeStruct {
	sites := SitesFoldersWithCompose()
	folders := []SitesFoldersWithComposeStruct{}
	for _, site := range sites {
		_, running := docker.FindContainerFromMountPoint(site.Path)
		if !running {
			folders = append(folders, site)
		}
	}
	return folders
}

// UserHomeDir : Returns the current users home dir or exits with error if not found
func UserHomeDir() string {
	home, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if home == "" {
		fmt.Println("Could not establish where your home directory is")
		os.Exit(1)
	}
	return home
}

// CWD : Current Working Directory
func CWD() string {
	cwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return cwd
}

// YamlParseDockerCompose : Takes a yaml string and returns a data struct of that string content
func YamlParseDockerCompose(path string) YamlDockerComposeStruct {
	t := YamlDockerComposeStruct{}
	if !PathExists(path) {
		return t
	}
	bytes, fileErr := ioutil.ReadFile(path)
	if fileErr != nil {
		return t
	}
	err := yaml.Unmarshal([]byte(bytes), &t)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	// if len(t.Services.Web.Ports) != 0 && t.Services.Web.Ports[0] != "" {
	// 	boundPorts := strings.Split(t.Services.Web.Ports[0], ":")
	// 	if len(boundPorts) != 0 {
	// 		t.Services.Web.Port = boundPorts[0]
	// 	}
	// }
	return t
}

// func GetComposeMappedValue(dotNotation string, composeMapped YamlDockerComposeMappedStruct) string {
// 	keys := strings.Split(dotNotation, ".")
// 	fmt.Println(keys)

// }
