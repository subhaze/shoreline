package run

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"shoreline/docker"
	"shoreline/fs"
	"shoreline/prompt"

	"github.com/spf13/cobra"
)

// Bash : Exec bash prompt within a container
func Bash(cmd *cobra.Command) (*exec.Cmd, error) {
	errMsg := "Could not find a container to run bash in."
	containerName := cmd.Flag("container").Value.String()
	if containerName != "" {
		return bashExecute(cmd, containerName)
	}
	prompt.SetPromptIcon(prompt.IconShell)
	container, found := docker.FindContainerFromMountPoint(fs.CurrentWorkingDir())
	if found {
		containerName = docker.GetContainerName(container)
		return bashExecute(cmd, containerName)
	}
	selectedApp, err := prompt.SelectStartedApp()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	container, found = docker.FindContainerFromMountPoint(selectedApp.Path)
	containerName = docker.GetContainerName(container)
	if found {
		return bashExecute(cmd, containerName)
	}
	fmt.Println(errMsg)
	return exec.Command("sh", "-c", "empty"), errors.New(errMsg)
}

func bashExecute(cmd *cobra.Command, containerName string) (*exec.Cmd, error) {
	return execute(cmd, fmt.Sprintf("docker exec -it %s /bin/bash", containerName))
}
