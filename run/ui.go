package run

import (
	"fmt"
	"net/http"
	"shoreline/api"

	"github.com/gobuffalo/packr"
	"github.com/spf13/cobra"
)

// UI : Starts http file server to serve Vue UI Application
func UI(cmd *cobra.Command) {
	port := cmd.Flag("port").Value.String()

	fmt.Println("Attempting to start Web UI at http://localhost:" + port)
	fmt.Println("use 'ctrl+c' to stop server")

	box := packr.NewBox("../static/dist")
	http.Handle("/", http.FileServer(box))

	api.SetupAPI(cmd)

	fmt.Println(http.ListenAndServe(":"+port, nil))
}
