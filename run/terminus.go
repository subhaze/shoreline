package run

import (
	"fmt"
	"os"
	"shoreline/docker"
	"shoreline/fs"
	"shoreline/prompt"
	"strings"

	"github.com/spf13/cobra"
)

func runTerminusNoLink(cmd *cobra.Command, args []string) {
	tag := cmd.Flag("image-tag").Value.String()
	execute(cmd, fmt.Sprintf("docker run -it --rm -e TERMINUS_USER_HOME=/tmp --volume %s:/tmp/.terminus --entrypoint=terminus tombras/terminus:%s %s",
		fs.UserHomeDir()+"/.terminus",
		tag,
		strings.Join(args, " "),
	))
}

// Terminus : Exec Terminus within a container
func Terminus(cmd *cobra.Command, args []string) {
	tag := cmd.Flag("image-tag").Value.String()
	linkContainer := cmd.Flag("link-container").Value.String()
	if linkContainer == "false" {
		runTerminusNoLink(cmd, args)
		return
	}
	prompt.SetPromptIcon("\U0001F689") // station
	containerName := ""

	// check for docker-compose in cwd
	container, found := docker.FindContainerFromMountPoint(fs.CurrentWorkingDir())
	if found {
		containerName = docker.GetContainerName(container)
	} else {
		// otherwise prompt for selecting a running container
		selectedApp, err := prompt.SelectStartedApp()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		container, found = docker.FindContainerFromMountPoint(selectedApp.Path)
		containerName = docker.GetContainerName(container)
	}
	containerEnvs := docker.GetContainerEnvString(containerName)
	if found {
		execute(cmd, fmt.Sprintf("docker run -it --rm --user 33:33 -e COMPOSER_HOME=/tmp -e TERMINUS_USER_HOME=/tmp %s --volume %s:/tmp --volumes-from %s --network container:%s --entrypoint=terminus tombras/terminus:%s %s",
			containerEnvs,
			fs.UserHomeDir(),
			containerName,
			containerName,
			tag,
			strings.Join(args, " "),
		))
	} else {
		fmt.Println("Could not find a container to run bash in.")
	}
}
