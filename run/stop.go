package run

import (
	"fmt"
	"os/exec"
	"shoreline/docker"

	"github.com/spf13/cobra"
)

// ContainerStop : Run `docker stop <ID>` to stop a running container
func ContainerStop(cmd *cobra.Command) (exec *exec.Cmd, err error) {
	containerID := cmd.Flag("container-id").Value.String()
	stopAll := cmd.Flag("all").Value.String()

	if stopAll == "true" {
		for _, container := range docker.GetRunningContainers() {
			exec, err = execute(cmd, fmt.Sprintf("docker stop %s", container.ID))
		}
		return
	}
	return execute(cmd, fmt.Sprintf("docker stop %s", containerID))
}
