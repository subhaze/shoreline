package run

import (
	"fmt"
	"os"
	"path/filepath"
	"shoreline/prompt"

	"github.com/spf13/cobra"
)

// ComposeUp : Run `docker-compose up -d` or `docker-compose -f <f> up -d` if non-empty string is passed in
func ComposeUp(cmd *cobra.Command) {
	composeFile := cmd.Flag("compose-file").Value.String()

	if cmd.Flag("prompt").Value.String() == "true" {
		prompt.SetPromptIcon("\U0001F388") // balloon
		selectedApp, err := prompt.SelectStoppedApp()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		composeFile = selectedApp.ComposeFile
		prompt.SetSelectedAppCache(selectedApp)
	}

	execute(cmd, fmt.Sprintf("docker-compose -f %s up -d", composeFile), filepath.Dir(composeFile))
}
