package run

import (
	"github.com/spf13/cobra"
)

// Reboot : Run ComposeDown() and then ComposeUp()
func Reboot(cmd *cobra.Command) {
	ComposeDown(cmd)
	ComposeUp(cmd)
}
