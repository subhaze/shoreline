package run

import (
	"fmt"
	"os"
	"os/user"
	"shoreline/docker"
	"shoreline/fs"
	"shoreline/prompt"
	"strings"

	"github.com/spf13/cobra"
)

// Composer : Exec Composer within a container
func Composer(cmd *cobra.Command, args []string) {
	tag := cmd.Flag("image-tag").Value.String()
	prompt.SetPromptIcon(prompt.IconMusicalScore)
	containerName := ""
	found := false
	// check for docker-compose in cwd
	container, found := docker.FindContainerFromMountPoint(fs.CurrentWorkingDir())
	if found {
		containerName = docker.GetContainerName(container)
	} else {
		// otherwise prompt for selecting a running container
		selectedApp, err := prompt.SelectStartedApp()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		container, found = docker.FindContainerFromMountPoint(selectedApp.Path)
		containerName = docker.GetContainerName(container)
		if !found {
			fmt.Println("Could not find a container to run bash in.")
			os.Exit(1)
		}
	}
	user, err := user.Current()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	cmdString := fmt.Sprintf("docker run --rm --interactive --tty %s --volume %s:/app --volume %s:/tmp --user %s:%s composer:%s %s",
		docker.GetContainerEnvString(containerName),
		docker.GetContainerMountPoint(container),
		fs.UserHomeDir(),
		user.Uid,
		user.Gid,
		tag,
		strings.Join(args, " "))
	execute(cmd, cmdString)
}
