package run

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
)

var cobraCmd *cobra.Command

// SetCobraCmd : Sec command for usage through out run package
// deprecated
func SetCobraCmd(cmd *cobra.Command) {
	cobraCmd = cmd
}

func getTerminalLength() int {
	cmdSize := exec.Command("stty", "size")
	cmdSize.Stdin = os.Stdin
	out, err := cmdSize.Output()
	sizesOutput := string(out)
	sizes := strings.Split(sizesOutput, " ")
	if err != nil || len(sizes) < 2 {
		return 10
	}
	length, err := strconv.Atoi(strings.Replace(sizes[1], "\n", "", -1))
	if err != nil {
		fmt.Println(err)
		return 10
	}
	return length
}

func execute(cobraCmd *cobra.Command, cmdStr string, directoryContext ...string) (command *exec.Cmd, err error) {
	command = exec.Command("sh", "-c", cmdStr)
	command.Stdout = os.Stdout
	command.Stdin = os.Stdin
	command.Stderr = os.Stderr
	if len(directoryContext) > 0 {
		command.Dir = directoryContext[0]
	}
	err = command.Run()
	if cobraCmd.Flag("show-docker-command").Value.String() == "true" {
		fmt.Println("\nDocker command ran")
		fmt.Println(strings.Repeat("-", getTerminalLength()))
		fmt.Println(cmdStr)
		fmt.Print(strings.Repeat("-", getTerminalLength()) + "\n\n")
	}
	return command, err
}
