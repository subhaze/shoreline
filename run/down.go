package run

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"shoreline/prompt"

	"github.com/spf13/cobra"
)

// ComposeDown : Run `docker-compose down` or `docker-compose -f <f> down` if non-empty string is passed in
func ComposeDown(cmd *cobra.Command) (*exec.Cmd, error) {
	composeFile := cmd.Flag("compose-file").Value.String()

	if cmd.Flag("prompt").Value.String() == "true" {
		prompt.SetPromptIcon(prompt.IconStopSign)
		selectedApp, err := prompt.SelectStartedApp()
		composeFile = selectedApp.ComposeFile
		prompt.SetSelectedAppCache(selectedApp)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	return execute(cmd, fmt.Sprintf("docker-compose -f %s down", composeFile), filepath.Dir(composeFile))
}
