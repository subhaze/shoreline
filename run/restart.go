package run

import (
	"fmt"
	"os"
	"shoreline/prompt"

	"github.com/spf13/cobra"
)

// ComposeRestart : Run `docker-compose restart` or `docker-compose -f <f> restart` if non-empty string is passed in
func ComposeRestart(cmd *cobra.Command) {
	composeFile := cmd.Flag("compose-file").Value.String()

	if cmd.Flag("prompt").Value.String() == "true" {
		prompt.SetPromptIcon(prompt.IconRecycle)
		selectedApp, err := prompt.SelectStartedApp()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		composeFile = selectedApp.ComposeFile
		prompt.SetSelectedAppCache(selectedApp)
	}
	execute(cmd, fmt.Sprintf("docker-compose -f %s restart", composeFile))
}
