package run

import (
	"fmt"
	"os/exec"

	"github.com/spf13/cobra"
)

// ContainerStats : Run `docker stats <ID> plus JSON formating`
func ContainerStats(cmd *cobra.Command) (exec *exec.Cmd, err error) {
	containerID := cmd.Flag("container-id").Value.String()
	return execute(cmd, fmt.Sprintf("docker stats %s --no-stream", containerID))
}
