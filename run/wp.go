package run

import (
	"fmt"
	"os"
	"shoreline/docker"
	"shoreline/fs"
	"shoreline/prompt"
	"strings"

	"github.com/spf13/cobra"
)

// WP : Exec WP-CLI within a container
func WP(cmd *cobra.Command, args []string) {
	tag := cmd.Flag("image-tag").Value.String()
	prompt.SetPromptIcon("\U0001F5DC") // clamp
	containerName := ""
	found := false

	// check for docker-compose in cwd
	container, found := docker.FindContainerFromMountPoint(fs.CurrentWorkingDir())
	if found {
		containerName = docker.GetContainerName(container)
	} else {
		// otherwise prompt for selecting a running container
		selectedApp, err := prompt.SelectStartedApp()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		container, found = docker.FindContainerFromMountPoint(selectedApp.Path)
		containerName = docker.GetContainerName(container)
	}
	containerEnvs := docker.GetContainerEnvString(containerName)
	if found {
		execute(cmd, fmt.Sprintf("docker run -it --rm --user 33:33 -e HTTP_HOST=localhost:65101 -e SERVER_NAME=localhost -e SERVER_PORT=443 %s --volumes-from %s --network container:%s wordpress:%s %s",
			containerEnvs,
			containerName,
			containerName,
			tag,
			strings.Join(args, " "),
		))
	} else {
		fmt.Println("Could not find a container to run bash in.")
	}
}
