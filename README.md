# shoreline

### Assumptions made by shoreline

- Your web app is in the docker-compose.yml file named `web:`
- Your web apps are found within your `~/Sites` folder when looking for apps to take action on.

### Documentation

[Full CLI Docs](docs/shoreline.md)

You can run shoreline from any directory, if no `docker-compose.yml` is found it will search
your `~/Sites` directory and list folders that container a `docker-compose.yml` file.

If you run shoreline within a directory that has a `docker-compose.yml` file it will assume
this is the app you want to take action on, you can bypass this via the `--prompt` flag.

#### Composer

When running composer commands, your home directory will be mounted to the composer container
to allow for caching and user:group 33 is passed in to mimic the same user as debian based systems (www-data).

#### Passing flags tools like wp or composer

When passing flags to other CLI tools such as WP-CLI or Composer you'll need to
separate those flags from shorelines's via ` -- `; Any flags after this are passed
to the underlying tool.

`shoreline wp -- --info` will run WP-CLI passing it the `--info` flag to WP-CLI

or

`shoreline composer -- --no-plugins --no-interaction`

#### Other Examples

Spin up an app via `shoreline up`

Spin down and app via `shoreline down`

Spin down an app in another directory `shoreline down --prompt`
