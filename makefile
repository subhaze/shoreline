docs = docs/*.md

all: shoreline $(docs)
.PHONY: all

$(docs): commands/*.go
	@SHORELINE_DOCS=true go run shoreline.go 1>/dev/null
	@echo "docs built"

shoreline: *.go */*.go
	go build shoreline.go
	chmod u+x shoreline
	cp shoreline /usr/local/bin

build: shoreline
.PHONY: build

ui:
	cd static && NODE_ENV=production npm run build
