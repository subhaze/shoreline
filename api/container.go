package api

import (
	"fmt"
	"net/http"
	"shoreline/docker"
	"shoreline/fs"

	"github.com/gorilla/mux"
)

func setupDockerRoutes(router *mux.Router) {
	router.Path("/containers/running").HandlerFunc(runningContainers).Methods("GET")
	router.Path("/containers/stopped").HandlerFunc(stoppedContainers).Methods("GET")
	router.Path("/containers/all").HandlerFunc(allContainers).Methods("GET")
	router.Path("/container/down").Queries("file", "{file}").HandlerFunc(containerDown).Methods("GET")
	router.Path("/container/up").Queries("file", "{file}").HandlerFunc(containerUp).Methods("GET")
	router.Path("/container/stop").Queries("containerId", "{containerId}", "all", "{all}").HandlerFunc(dockerDown).Methods("GET")
	router.Path("/container/stats").Queries("containerId", "{containerId}").HandlerFunc(dockerStats).Methods("GET")
	router.Path("/container/cli").Queries("container", "{container}").HandlerFunc(cliContainer)
}

func runningContainers(w http.ResponseWriter, r *http.Request) {
	jsonResponse(w, docker.GetRunningContainers())
}

func stoppedContainers(w http.ResponseWriter, r *http.Request) {
	jsonResponse(w, fs.SitesFoldersWithComposeStopped())
}

func allContainers(w http.ResponseWriter, r *http.Request) {
	jsonResponse(w, fs.SitesFoldersWithCompose())
}

func containerDown(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	file := vars["file"]
	rootCobra.SetArgs([]string{
		"down",
		fmt.Sprintf("--compose-file=%s", file),
	})
	rootCobra.Execute()
	jsonResponse(w, "attempting to shut down")
}

func containerUp(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	file := vars["file"]
	rootCobra.SetArgs([]string{
		"up",
		fmt.Sprintf("--compose-file=%s", file),
	})
	rootCobra.Execute()
	jsonResponse(w, "done")
}

func dockerDown(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	containerID := vars["containerId"]
	all := vars["all"]
	rootCobra.SetArgs([]string{
		"stop",
		fmt.Sprintf("--container-id=%s", containerID),
		fmt.Sprintf("--all=%s", all),
	})
	rootCobra.Execute()
	jsonResponse(w, "stopped?")
}

func dockerStats(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	containerID := vars["containerId"]
	stats := docker.GetContainerStats(containerID)
	createWebSocketConn(w, r, stats.Body)
}

// var upgrader = websocket.Upgrader{} // use default options
func cliContainer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	containerID := vars["container"]
	h, _ := w.(http.Hijacker)
	c, _, _ := h.Hijack()
	hj, _ := docker.GetContainerWebSocket(containerID)
	c = hj.Conn
	defer c.Close()
	defer hj.Close()
}
