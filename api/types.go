package api

// Folder : test stuff
type Folder struct {
	Name       string `json:"name"`
	HasProject bool   `json:"hasProject"`
}
