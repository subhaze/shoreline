package api

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	// "golang.org/x/net/websocket"

	"github.com/spf13/cobra"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

var router = mux.NewRouter()
var commands = make(map[string]func(*cobra.Command, ...string))
var upgrader = websocket.Upgrader{} // use default options
var rootCobra = &cobra.Command{
	Version: "0.1",
	Use:     "shoreline",
	Short:   "shoreline: CLI tool to help with running docker commands",
	Long: `This tool is here to just slightly wrap docker and docker-compose cli
to help run commands a bit more easily, such as:

- Spinning up containers inside/outside of your current working dir
- Shutting down containers inside/outside of your current working dir
- Running WP-CLI against a container sharing ENV variables and networking
- Running Composer against a codebase
- Entering the bash shell on a running container
`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// SetupAPI : initiates API routes
func SetupAPI(cmd *cobra.Command) {
	rootCobra = cmd.Root()
	// rootCobra.ResetFlags()
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	setupFolderRoutes(router)
	setupDockerRoutes(router)

	// router.Path("/cli/flags").HandlerFunc(cliFlags).Methods("GET")

	apiHTTPHandler := http.StripPrefix("/api", router)
	http.Handle("/api/", handlers.CORS(headersOk, originsOk, methodsOk)(apiHTTPHandler))
}

func jsonResponse(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	// fmt.Println(json.NewEncoder(w).Encode(data))
	json.NewEncoder(w).Encode(data)
}

func createWebSocketConn(w http.ResponseWriter, r *http.Request, reader io.ReadCloser) {
	upgrader.CheckOrigin = func(r *http.Request) bool {
		return strings.HasPrefix(r.Host, "localhost")
	}
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		jsonResponse(w, err)
	}
	scanner := bufio.NewScanner(reader)
	scanner.Split(bufio.ScanLines)
	go cleanUpWebSocketConn(c, reader)
	for scanner.Scan() {
		fmt.Println("writting to socket")
		c.WriteMessage(1, scanner.Bytes())
	}
}

// used to clean up websocket connections and any reader associated to it
func cleanUpWebSocketConn(c *websocket.Conn, reader io.ReadCloser) {
	for {
		if _, _, err := c.NextReader(); err != nil {
			fmt.Println(err)
			c.Close()
			reader.Close()
			break
		}
	}
}
