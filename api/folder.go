package api

import (
	"net/http"
	"shoreline/fs"

	"github.com/gorilla/mux"
)

func setupFolderRoutes(router *mux.Router) {
	router.Path("/folder/list").Queries("path", "{path}").HandlerFunc(folderList).Methods("GET")
	router.Path("/folder/home").HandlerFunc(homeFolder).Methods("GET")
	router.Path("/folder/cwd").HandlerFunc(workingDirectory).Methods("GET")
}

func homeFolder(w http.ResponseWriter, r *http.Request) {
	jsonResponse(w, fs.UserHomeDir())
}

func workingDirectory(w http.ResponseWriter, r *http.Request) {
	jsonResponse(w, fs.CWD())
}

func folderList(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	jsonResponse(w, fs.FoldersFromPath(vars["path"]))
}
