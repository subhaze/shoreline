// Copyright (c) 2018-present Michael Russell <jmrussell22@gmail.com>
// This code is licensed under MIT license (see LICENSE for details)

package commands

import (
	"shoreline/flags"
	"shoreline/run"

	"github.com/spf13/cobra"
)

// upCmd represents the up command
var upCmd = &cobra.Command{
	Use:   "up",
	Short: "Runs 'docker-compose up -d' if app is not running.",
	Long: `Will run 'docker-compose up -d' in current directory if app is not running.
If no docker-compose.yml file exists or is already running a prompt listing
out non-running apps found within '~/Sites' will display to allow running of other apps.
`,
	Run: func(cmd *cobra.Command, args []string) {
		run.ComposeUp(cmd)
	},
}

func init() {
	rootCmd.AddCommand(upCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// upCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// fmt.Println(docker.GetRunningContainers())
	flags.AddComposeFile(upCmd)
}
