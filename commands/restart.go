// Copyright (c) 2018-present Michael Russell <jmrussell22@gmail.com>
// This code is licensed under MIT license (see LICENSE for details)

package commands

import (
	"shoreline/flags"
	"shoreline/run"

	"github.com/spf13/cobra"
)

// restartCmd represents the restart command
var restartCmd = &cobra.Command{
	Use:   "restart",
	Short: "Restarts all stopped and running services. (won't reload docker-compose.yml)",
	Long: `If you make changes to your docker-compose.yml configuration these changes
are not reflected after running this command.

For example, changes to environment variables (which are
added after a container is built, but before the container’s
command is executed) are not updated after restarting.`,
	Run: func(cmd *cobra.Command, args []string) {
		run.ComposeRestart(cmd)
	},
}

func init() {
	rootCmd.AddCommand(restartCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// restartCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// restartCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	flags.AddComposeFile(restartCmd)
}
