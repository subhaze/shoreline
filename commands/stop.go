// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package commands

import (
	"shoreline/flags"
	"shoreline/run"

	"github.com/spf13/cobra"
)

// stopCmd represents the stop command
var stopCmd = &cobra.Command{
	Use:   "stop",
	Short: "Stop a running container, or all containers",
	Long: `Command to easily stop a running container or stop all running containers.
	
	Note: some containers could be configured in the docker-compose to automatically
	restart upon stopping. If you run into this situation you'll need to investigate
	how the container is started and change the restart policy
	to prevent it from continuing to restart upon stopping it.`,
	Run: func(cmd *cobra.Command, args []string) {
		run.ContainerStop(cmd)
	},
}

func init() {
	rootCmd.AddCommand(stopCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// stopCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// stopCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	flags.AddContainerID(stopCmd)
	stopCmd.Flags().Bool("all", false, "Stop all containers")
}
