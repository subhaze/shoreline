// Copyright (c) 2018-present Michael Russell <jmrussell22@gmail.com>
// This code is licensed under MIT license (see LICENSE for details)

package commands

import (
	"shoreline/run"

	"github.com/spf13/cobra"
)

// terminusCmd represents the terminus command
var terminusCmd = &cobra.Command{
	Use:   "terminus",
	Short: "Run terminus within a container",
	Long: `You can use this to interact with Pantheon.

Use Terminus to perform these and other operations:

- Create a new site
- Create and delete Multidev environments
- Clone one environment to another
- Check for and apply upstream updates
- Deploy code from one environment to another
- Run Drush and WP-CLI commands

Docs: https://pantheon.io/docs/terminus/
`,
	Run: func(cmd *cobra.Command, args []string) {
		run.Terminus(cmd, args)
	},
}

func init() {
	rootCmd.AddCommand(terminusCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// terminusCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// terminusCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	terminusCmd.Flags().Bool("link-container", false, "Execute terminus within a running container (defaults to just running in cwd without linking to a container)")
	addImageTagFlag(terminusCmd, "latest", "Override tombras/terminus tag to use a different version of terminus")
}
