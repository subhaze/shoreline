// Copyright (c) 2018-present Michael Russell <jmrussell22@gmail.com>
// This code is licensed under MIT license (see LICENSE for details)

package commands

import (
	"shoreline/flags"
	"shoreline/run"

	"github.com/spf13/cobra"
)

// rebootCmd represents the reboot command
var rebootCmd = &cobra.Command{
	Use:   "reboot",
	Short: "Stops containers and then starts them again (will reload docker-compose.yml)",
	Long: `Use this command if you need to reload updates to your docker-compose.yml file
or if there's any weird issues you may run into, as this will completely tear down the
current dev environment save for anything in your working directory.`,
	Run: func(cmd *cobra.Command, args []string) {
		run.Reboot(cmd)
	},
}

func init() {
	rootCmd.AddCommand(rebootCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// rebootCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// rebootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	flags.AddComposeFile(rebootCmd)
}
