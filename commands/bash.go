// Copyright (c) 2018-present Michael Russell <jmrussell22@gmail.com>
// This code is licensed under MIT license (see LICENSE for details)

package commands

import (
	"shoreline/flags"
	"shoreline/run"

	"github.com/spf13/cobra"
)

// bashCmd represents the bash command
var bashCmd = &cobra.Command{
	Use:   "bash",
	Short: "Enter bash shell in running container",
	Long: `If within a folder with a docker-compose.yml it will check to see if that container
is running, if so it will place you within it's bash shell;
If not it will look for other running containers and list
available containers run bash within.`,
	Run: func(cmd *cobra.Command, args []string) {
		run.Bash(cmd)
	},
}

func init() {
	rootCmd.AddCommand(bashCmd)
	flags.AddContainer(bashCmd)
}
