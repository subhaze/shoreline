// Copyright (c) 2018-present Michael Russell <jmrussell22@gmail.com>
// This code is licensed under MIT license (see LICENSE for details)

package commands

import (
	"shoreline/flags"
	"shoreline/run"

	"github.com/spf13/cobra"
)

// downCmd represents the down command
var downCmd = &cobra.Command{
	Use:   "down",
	Short: "Runs 'docker-compose down' if app is running.",
	Long: `Will run 'docker-compose down' in current directory if app is running.
If no docker-compose.yml file exists or is already stopped a prompt listing
out running apps found within '~/Sites' will display to allow stopping of other apps.
`,
	Run: func(cmd *cobra.Command, args []string) {
		run.ComposeDown(cmd)
	},
}

func init() {
	rootCmd.AddCommand(downCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// downCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	flags.AddComposeFile(downCmd)
}
