// Copyright (c) 2018-present Michael Russell <jmrussell22@gmail.com>
// This code is licensed under MIT license (see LICENSE for details)

package commands

import (
	"shoreline/run"

	"github.com/spf13/cobra"
)

// composerCmd represents the composer command
var composerCmd = &cobra.Command{
	Use:   "composer",
	Short: "Run PHP's composer package manager against a codebase",
	Long: `Run composer from Docker image, it will mount your home folder to
allow for caching of assets. Currently no container env variables are shared
and it's ran within its own private network.

Any flags you want to pass to it must come after '--' which is used to inform
shoreline to not process those flags as its own.

Docs: https://getcomposer.org/doc/01-basic-usage.md
`,
	Run: func(cmd *cobra.Command, args []string) {
		run.Composer(cmd, args)
	},
}

func init() {
	rootCmd.AddCommand(composerCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// composerCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// composerCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	addImageTagFlag(composerCmd, "latest", "Override composer:latest tag to use a different version of composer")
}
