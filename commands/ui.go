// Copyright (c) 2018-present Michael Russell <jmrussell22@gmail.com>
// This code is licensed under MIT license (see LICENSE for details)

package commands

import (
	"shoreline/run"

	"github.com/spf13/cobra"
)

// uiCmd represents the ui command
var uiCmd = &cobra.Command{
	Use:   "ui",
	Short: "Spin up local http server to server web UI app",
	Long: `Allows you to interact with shoreline via a web app.
	
	This is currently under development and doesn't do much right now :)`,
	Run: func(cmd *cobra.Command, args []string) {
		run.UI(cmd)
	},
}

func init() {
	rootCmd.AddCommand(uiCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// uiCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// uiCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	uiCmd.Flags().String("port", "3001", "Use an alternate port to server web UI.")
}
