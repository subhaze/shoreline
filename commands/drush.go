// Copyright (c) 2018-present Michael Russell <jmrussell22@gmail.com>
// This code is licensed under MIT license (see LICENSE for details)

package commands

import (
	"fmt"

	"github.com/spf13/cobra"
)

// drushCmd represents the drush command
var drushCmd = &cobra.Command{
	Use:   "drush",
	Short: "TODO: Currently does nothing",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("drush called")
	},
}

func init() {
	rootCmd.AddCommand(drushCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// drushCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// drushCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
