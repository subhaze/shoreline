// Copyright (c) 2018-present Michael Russell <jmrussell22@gmail.com>
// This code is licensed under MIT license (see LICENSE for details)

package commands

import (
	"shoreline/run"

	"github.com/spf13/cobra"
)

// wpCmd represents the wp command
var wpCmd = &cobra.Command{
	Use:   "wp",
	Short: "Run WP-CLI within a container",
	Long: `Runs wp within a container, it shares the web: volume of your compose file,
get env variables you've defined in the compose file, and shares the same network as
the compose file that it's triggered against.
	
Any flags you want to pass to it must come after '--' which is used to inform
shoreline to not process those flags as its own.

Docs: https://developer.wordpress.org/cli/commands/
`,
	Run: func(cmd *cobra.Command, args []string) {
		run.WP(cmd, args)
	},
}

func init() {
	rootCmd.AddCommand(wpCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// wpCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// wpCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	addImageTagFlag(wpCmd, "cli", "Override wordpress:cli tag to use a different version of WP-CLI")
}
