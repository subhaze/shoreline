// Copyright (c) 2018-present Michael Russell <jmrussell22@gmail.com>
// This code is licensed under MIT license (see LICENSE for details)

package commands

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"shoreline/run"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
	"github.com/spf13/viper"
)

var cfgFile string
var selectApp bool
var showDockerCommand bool
var forceComposeUp bool
var imageTag string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Version: "0.1",
	Use:     "shoreline",
	Short:   "shoreline: CLI tool to help with running docker commands",
	Long: `This tool is here to just slightly wrap docker and docker-compose cli
to help run commands a bit more easily, such as:

- Spinning up containers inside/outside of your current working dir
- Shutting down containers inside/outside of your current working dir
- Running WP-CLI against a container sharing ENV variables and networking
- Running Composer against a codebase
- Entering the bash shell on a running container
`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	} else {
		genDocks(rootCmd)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().BoolVarP(&selectApp, "prompt", "p", false, "Always display selection prompt of available containers for command.")
	rootCmd.PersistentFlags().BoolVarP(&showDockerCommand, "show-docker-command", "s", false, "Prints Docker command used to execute request.")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().BoolP("select", "s", false, "Always display selection prompt of available containers for command.")
	run.SetCobraCmd(rootCmd)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	home, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	workingDir, err := os.Getwd()
	if err == nil {
		viper.AddConfigPath(workingDir)
	}

	// Search config in home directory with name ".shoreline" (without extension).
	viper.AddConfigPath(home)
	viper.SetConfigName(".shoreline")
	errNoConfig := viper.ReadInConfig()
	if errNoConfig != nil {
		err := viper.WriteConfigAs(filepath.Join(home, "/.shoreline.yml"))
		fmt.Println(err)
		os.Exit(1)
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		// fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func genDocks(cmd *cobra.Command) {
	// generate documentation for all commands
	if os.Getenv("SHORELINE_DOCS") != "" {
		err := doc.GenMarkdownTree(rootCmd, "./docs")
		if err != nil {
			log.Fatal(err)
		}
	}
}

// helper function to add flag to override what image to use when using 3rd images to execute tasks
// i.e. composer, WP-CLI, etc...
func addImageTagFlag(cmd *cobra.Command, defaultTag string, msg string) {
	cmd.Flags().StringP("image-tag", "t", defaultTag, msg)
}
