package flags

import (
	"github.com/spf13/cobra"
)

var flags Flags

func init() {
	// fmt.Println("command types.go")
}

// GetFlags : Returns all flags available in application
func GetFlags() Flags {
	return flags
}

// AddComposeFile : add flag to allow overriding default docker-compose.yml file
func AddComposeFile(cmd *cobra.Command) {
	cmd.Flags().StringP("compose-file", "f", "docker-compose.yml", "Pass a file name to use a different docker-compose.yml file")
}

// AddContainer : add flag to allow overriding default container to run command on
func AddContainer(cmd *cobra.Command) {
	cmd.Flags().String("container", "", "pass a container name to execute command on")
}

// AddContainerID : add flag to allow overriding default container to run command on
func AddContainerID(cmd *cobra.Command) {
	cmd.Flags().String("container-id", "", "pass a container id to execute command on")
}

// SetFlag : define an application command flag, used to help generate api JSON data of available flags
func SetFlag(cmd *cobra.Command, flag Flag) {
	cmdName := cmd.Name()
	if flags == nil {
		flags = make(Flags, 0)
	}
	if _, exists := flags[cmdName]; !exists {
		flags[cmdName] = []Flag{}
	}
	flags[cmdName] = append(flags[cmdName], flag)
}
