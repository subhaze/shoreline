package flags

// Flag : struct for defining application flag
type Flag struct {
	Name       string
	Shorthand  string
	Value      string
	ValueType  string
	Usage      string
	Persistent bool
}

// Flags : struct for handling application flags
type Flags map[string][]Flag
