import { Vue } from "vue-property-decorator";
// import Vue from "vue";
import { Store } from "vuex";
import { RootState } from "../store/types";

export default abstract class VueBase extends Vue {
	public $store!: Store<RootState>;
}
