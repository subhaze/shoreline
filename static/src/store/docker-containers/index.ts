import { VuexModule, Module, Mutation, Action } from "vuex-class-modules";
import { ContainerProjectHash, Container } from "./types";
import { API_URL } from "../helper";
import store from "../index";
import axios from "axios";

@Module({ generateMutationSetters: true })
export class DockerContainers extends VuexModule {
	public containers: Container[] = [];

	@Mutation
	public updateContainers(containers: Container[]) {
		this.containers = containers;
	}

	@Action
	public dispatchGetContainers(): void {
		axios
			.get<Container[]>(`${API_URL()}/containers/running`)
			.then(r => this.updateContainers(r.data));
	}

	get containersGroupedByProject(): ContainerProjectHash {
		const tmp: ContainerProjectHash = {};
		const unknownContainers: Container[] = this.containers.filter(
			c => !c.Labels["com.docker.compose.project"]
		);
		const knownContainers: Container[] = this.containers.filter(
			c => !!c.Labels["com.docker.compose.project"]
		);
		unknownContainers.forEach(c => {
			const hostMountPaths = c.Mounts.map(m => m.Source);
			for (const c2 of knownContainers) {
				const hostMountPaths2 = c2.Mounts.map(m => m.Source);
				if (hostMountPaths.some(m => hostMountPaths2.includes(m))) {
					c.Labels["com.docker.compose.project"] =
						c2.Labels["com.docker.compose.project"];
					knownContainers.push(c);
					break;
				}
			}
		});
		knownContainers.forEach(c => {
			if (!(c.Labels["com.docker.compose.project"] in tmp)) {
				tmp[c.Labels["com.docker.compose.project"]] = [];
			}
			tmp[c.Labels["com.docker.compose.project"]].push(c);
		});
		return tmp;
	}
}

export const dockerContainers = new DockerContainers({
	store,
	name: "DockerContainers"
});
