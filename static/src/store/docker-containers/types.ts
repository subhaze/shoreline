export interface Container {
	Id: string;
	Image: string;
	Command: string;
	Created: number;
	State: string;
	Status: string;
	Ports: Port[];
	Names: string[];
	Labels: {
		"com.docker.compose.config-hash": string;
		"com.docker.compose.container-number": string;
		"com.docker.compose.oneoff": string;
		"com.docker.compose.project": string;
		"com.docker.compose.service": string;
		"com.docker.compose.version": string;
	};
	NetworkSettings: {
		Networks: {
			[key: string]: Network;
		};
	};
	Mounts: Mount[];
}

interface Mount {
	Source: string;
	Destination: string;
	Mode: string;
	RW: boolean;
}

interface Network {
	MacAddress: string;
	IPPrefixLen: number;
	IPAddress: string;
	Gateway: string;
	EndpointID: string;
	NetworkID: string;
}

export interface Port {
	PrivatePort: number;
	PublicPort?: number;
	Type: string;
	IP: string;
}

export interface ContainerProjectHash {
	[projectName: string]: Container[];
}

export interface DockerContainersState {
	containers: Container[];
}

export interface ContainerStats {
	cpu_stats: {
		system_cpu_usage: number;
		online_cpus: number;
		cpu_usage: {
			total_usage: number;
		};
	};
	precpu_stats: {
		system_cpu_usage: number;
		online_cpus: number;
		cpu_usage: {
			total_usage: number;
		};
	};
	memory_stats: {
		limit: number;
		max_usage: number;
		usage: number;
	};
}
