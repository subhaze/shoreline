import { FileSystemState } from "./file-system/types";
import { DockerContainersState } from "./docker-containers/types";

export interface RootState {
	version: string;
	fileSystem: FileSystemState;
	dockerContainers: DockerContainersState;
}
