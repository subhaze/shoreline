export interface BaseFolder {
	name: string;
	path: string;
	hasCompose: boolean;
	hidden: boolean;
}
export interface FileSystemState {
	cwd: string;
	home: string;
	folders: BaseFolder[];
}
