import { VuexModule, Module, Mutation, Action } from "vuex-class-modules";
import { API_URL } from "../helper";
import { BaseFolder } from "./types";
import axios from "axios";
import store from "../index";

@Module({ generateMutationSetters: true })
export class FileSystem extends VuexModule {
	public cwd = "";
	public home = "";
	public folders: BaseFolder[] = [];

	@Mutation
	public updateCwd(cwd: string) {
		this.cwd = cwd;
	}
	@Mutation
	public updateHome(home: string) {
		this.home = home;
	}
	@Mutation
	public updateFolders(folderList: BaseFolder[]) {
		this.folders = folderList;
	}

	@Action
	public dispatchCWD(): void {
		axios.get(`${API_URL()}/folder/cwd`).then(r => this.updateCwd(r.data));
	}
	@Action
	public dispatchHome(): void {
		axios.get(`${API_URL()}/folder/home`).then(r => this.updateHome(r.data));
	}
	@Action
	public dispatchFolderList(path: string): void {
		axios
			.get(`${API_URL()}/folder/list?path=${encodeURIComponent(path)}`)
			.then(r => {
				this.updateFolders(r.data);
				this.updateCwd(path);
			});
	}
}

export const fileSystem = new FileSystem({
	store,
	name: "FileSystem"
});
