// import Vue from "vue";
// import Vuex from "vuex";
// import { RootState } from "./types";
// import { fileSystem } from "./file-system";
// import { dockerContainers } from "./docker-containers";

// Vue.use(Vuex);

// export default new Vuex.Store<RootState>({
// 	// state: {},
// 	// mutations: {},
// 	// actions: {}
// 	modules: {
// 		fileSystem,
// 		dockerContainers
// 	}
// });

import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
const store = new Vuex.Store({
	strict: process.env.NODE_ENV !== "production"
});

export default store;
