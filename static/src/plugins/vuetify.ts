import Vue from "vue";
import Vuetify from "vuetify/lib";
import "vuetify/src/stylus/app.styl";

Vue.use(Vuetify, {
	iconfont: "md",
	theme: {
		primary: "#607d8b",
		secondary: "#ff5722",
		accent: "#009688",
		error: "#f44336",
		warning: "#ffc107",
		info: "#795548",
		success: "#cddc39"
	}
});
