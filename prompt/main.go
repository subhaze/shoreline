package prompt

import (
	"fmt"
	"shoreline/fs"
	"strings"

	"github.com/manifoldco/promptui"
)

var cachedSelectedApp fs.SitesFoldersWithComposeStruct
var promptIcon string

func hasCache() bool {
	return cachedSelectedApp.Name != ""
}

// SetPromptIcon : Allows adjustment of list icon used when prompting for app selection
func SetPromptIcon(icon string) {
	promptIcon = icon
}

// SetSelectedAppCache : Useful to save a looked up app to avoid re-promting for app
func SetSelectedAppCache(app fs.SitesFoldersWithComposeStruct) {
	cachedSelectedApp = app
}

// ClearSelectedAppCache : Clear out previously cached app to enforce re-prompting user for app
func ClearSelectedAppCache() {
	cachedSelectedApp = fs.SitesFoldersWithComposeStruct{}
}

// SelectStoppedApp : Prompt for selecting an app that is not known to be running and return it
func SelectStoppedApp() (fs.SitesFoldersWithComposeStruct, error) {
	if hasCache() {
		return cachedSelectedApp, nil
	}
	apps := fs.SitesFoldersWithComposeStopped()
	templates := &promptui.SelectTemplates{
		Label:    "{{ . }}",
		Active:   promptIcon + " {{ .Name | cyan }}",
		Inactive: "  {{ .Name | cyan }}",
		Selected: promptIcon + " {{ .Name | red | cyan }}",
		Details: `
--------- App ----------
{{ "Name:" | faint }}	{{ .Name }}
{{ "Location:" | faint }}	{{ .Path }}
{{ "Compose File:" | faint }}	{{ .ComposeFile }}`,
		// {{ "Port:" | faint }}	{{ .Compose.Services.Web.Port }}`,
	}

	searcher := func(input string, index int) bool {
		app := apps[index]
		name := strings.Replace(strings.ToLower(app.Name), " ", "", -1)
		input = strings.Replace(strings.ToLower(input), " ", "", -1)

		return strings.Contains(name, input)
	}

	prompt := promptui.Select{
		Label:     "Please select an App to start up" + "\n\n",
		Items:     apps,
		Templates: templates,
		Size:      4,
		Searcher:  searcher,
	}

	i, _, err := prompt.Run()

	if err != nil {
		if err.Error() == "^C" {
			fmt.Printf("Prompt exited by user via: ")
		} else {
			fmt.Printf("Prompt failed %v\n", err)
		}
		return fs.SitesFoldersWithComposeStruct{}, err
	}

	// fmt.Printf("Running command in context of: %s", apps[i].Path)
	return apps[i], nil
}

// SelectStartedApp : Prompt for selecting an app that is known to be running and return it
func SelectStartedApp() (fs.SitesFoldersWithComposeStruct, error) {
	if hasCache() {
		return cachedSelectedApp, nil
	}
	apps := fs.SitesFoldersWithComposeRunning()
	templates := &promptui.SelectTemplates{
		Label:    "{{ . }}",
		Active:   promptIcon + " {{ .Name | cyan }}",
		Inactive: "  {{ .Name | cyan }}",
		Selected: promptIcon + " {{ .Name | red | cyan }}",
		Details: `
--------- App ----------
{{ "Name:" | faint }}	{{ .Name }}
{{ "Location:" | faint }}	{{ .Path }}
{{ "Compose File:" | faint }}	{{ .ComposeFile }}`,
		// {{ "Port:" | faint }}	{{ .Compose.Services.Web.Port }}`,
	}

	searcher := func(input string, index int) bool {
		app := apps[index]
		name := strings.Replace(strings.ToLower(app.Name), " ", "", -1)
		input = strings.Replace(strings.ToLower(input), " ", "", -1)

		return strings.Contains(name, input)
	}

	prompt := promptui.Select{
		Label:     "Please select an App" + "\n\n",
		Items:     apps,
		Templates: templates,
		Size:      4,
		Searcher:  searcher,
	}

	i, _, err := prompt.Run()

	if err != nil {
		if err.Error() == "^C" {
			fmt.Printf("Prompt exited by user via: ")
		} else {
			fmt.Printf("Prompt failed %v\n", err)
		}
		return fs.SitesFoldersWithComposeStruct{}, err
	}

	// fmt.Printf("Running command in context of: %s", apps[i].Path)
	return apps[i], nil
}
