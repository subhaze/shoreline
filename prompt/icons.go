package prompt

// IconStopSign : Font icon
var IconStopSign = "\U0001F6D1"

// IconShell : Font icon
var IconShell = "\U0001F41A"

// IconMusicalScore : Font icon
var IconMusicalScore = "\U0001F3BC"

// IconStation : Font icon
var IconStation = "\U0001F689"

// IconBalloon : Font icon
var IconBalloon = "\U0001F388"

// IconClamp : Font icon
var IconClamp = "\U0001F5DC"

// IconRecycle : Font icon
var IconRecycle = "\U0000267C"
