package template

import (
	"fmt"
)

// WPCompose : Generate base docker-compose for wordpress
func WPCompose() {
	baseTemplate := `
version: '3.5'
services:
  web:
    image: tombras/wordpress
    depends_on:
      - db
    ports:
      - "8900:80"
    volumes:
      - .:/var/www/html
    environment:
      WP_HOME: http://localhost:8900
      WP_SITEURL: http://localhost:8900
      DB_NAME: wp_first_farmers
      DB_USER: root
      DB_PASSWORD: root
      DB_HOST:  mysql-server-first-farmers:3306
      CACHE: 0
      WP_DEBUG: 1
  phpmyadmin:
    image: phpmyadmin/phpmyadmin:4.7
    depends_on:
      - db
    ports:
      - 8901:80
    environment:
      PMA_HOST: db
  db:
    image: mysql:5.7
    volumes:
      - ./docker-persistent-data/mysql-data:/var/lib/mysql
      - ./docker-persistent-data/mysql-logs:/var/log/mysql
      - ./docker-persistent-data/mysql.cnf:/etc/mysql/mysql.conf.d/extended.cnf
    # ports:
    #   - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: wp_first_farmers

`
	fmt.Println(baseTemplate)
}
